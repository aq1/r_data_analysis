orange <- read.table("http://factominer.free.fr/book/orange.csv",header=TRUE, sep=";", dec=".", row.names=1)
summary(orange)
install.packages("FactoMineR", "./")
path.package()
library()
.libPaths("./")
library(FactoMineR)

# Calculation & output the graph in the variable space
res.pca <- PCA(orange,quanti.sup=8:14,quali.sup=15:16)

# plot of the graph of the subjects in the subject space
plot(res.pca, invisible = "quali")

# Coordinates of variables in the first 2 dimensions
round(res.pca$var$coord[,1:2],2)

# principal components with their inertia
round(res.pca$eig,2)

# distinace of individuals to the center
round(res.pca$ind$dist,2)

# Contributions from individuals to the dimensions
round(res.pca$ind$contrib[,1:2],2)

# Contributions from the variables to the dimensions
round(res.pca$var$contrib[,1:2],2)

lapply(dimdesc(res.pca),lapply,round,2)

# Househould spending data

library(FactoMineR)
don <- read.table("http://factominer.free.fr/livreV2/base_conso.csv",
                  header=TRUE,
                  sep=";",
                  row.names = 1,
                  check.names = FALSE)